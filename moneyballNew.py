import numpy as np
import pandas as pd
import os, sys
import random

try:
    os.chdir(sys.path[0])
except:
    os.chdir("/Users/dusty/moneyball")

players = pd.read_csv('playersheet.csv', header=None)

def startmenu():

    menu = int(input("\nPlease choose one of the following options:\n1) Season with Playoffs\n2) Season without Playoffs\n3) Exit\n\n"))

    if menu==1:
        season = seasonSim(players)
        tourneySim(season)
    else:
        quit()

def seasonSim(players):

    teamNo = int(input("\nPlease enter the number of teams to participate (between 2 and 10):\n"))
    print(str(teamNo) + " teams will play this season\n\n")

    capTotal = int(input("\nPlease choose a salary cap:\n"))
    print(str(capTotal) + " is the salary cap this season\n\n")


    order = list(range(1,teamNo+1))
    rOrder = np.random.shuffle(order)
    draft = np.zeros([teamNo,9])

    payrolls = np.zeros([teamNo])
    selections = []

    print("Draft order: " + str(order))

    for i in range(9):
        if i%2==0:
            for j in range(teamNo):
                print("\nTeam " + str(rOrder[j]) + "will now choose:\n")
                if payrolls[rOrder[j]] >= capTotal:
                    print("Cap exceeded. Adding Mendoza Player to Team " + str(rOrder[j]) + "\n")
                else:
                    print("Team " + str(rOrder[j]) + "'''s current payroll is " + str(payrolls[rOrder[j]]) + "\n")
                    draft = draftPick(draft, selections)



def draftPick(players, selections):
        temp = input("\nPlease choose a player:\n")
        if temp in selections:
            print("Invalid selection. Please try again:\n")
            temp = draftPick(players, selections)
        elif not (temp in players[0]):
            print("Invalid selection. Please try again:\n")
            temp = draftPick(players, selections)
        else:
            return temp


def gameSim(team1, team2):

    return None

def seriesSim(team1, team2, games):

    return None


def tourneySim(season):

    return None

startmenu()
