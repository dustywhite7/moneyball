\section*{\centering Appendix}
\subsection*{AI: Example of Project Instructions}
The following is an example of the material posted to a course management site that details the project that students must complete. The instructions below were written for students in a labor economics course. The instructions can be changed and tailored depending on whether it is intended for a sports management course or if group work is desired.

\begin{center}

{\bf Econ XXX \emph{Moneyball} Project} \newline
{\bf Player Evaluation Activity}
\end{center}

\subsubsection*{The Basics}
This assignment is due to TurnItIn.com and ANGEL before the beginning of class on Monday, March 16. The paper should be typed in 12 pt. font, double spaced, and the whole assignment should be contained in one single file, including copies of all supporting material identified in \#7 below. Do not ask me to accept assignments that are not combined, and please do not exceed the space limit of three written pages–my grading will stop there. You will submit the memo to TurnItIn.com and your data file to a dropbox in ANGEL.

We will discuss this assignment in class, so on the due date please have computer-readable copies of your graphs available, either on a CD, flash drive, or via e-mail. You may be invited to show them to the class and describe them briefly. As with all assignments, this is to be independent work.

\subsubsection*{Objective}
In this assignment we will assess the value of Major League Baseball (MLB) free agent batters signed at the beginning of the 2014 season, using a simplified version of the methods described in the article ``An Economic Evaluation of the \emph{Moneyball} Hypothesis." Six web sites will be of use in doing so:
\begin{itemize}
\item Player Salary Database: \url{http://www.usatoday.com/sports/mlb/salaries/2015/player/all/}
\item Player Performance Database: \url{http://www.baseball-almanac.com/players/ballplayer.shtml}
\item Free Agent Players: \url{http://www.spotrac.com/mlb/free-agents/}
\item Team Batting Statistics: \url{http://espn.go.com/mlb/stats/team/_/stat/batting}
\item Team Attendance Statistics: \url{http://espn.go.com/mlb/attendance/_/year/2014}
\item Team Average Ticket Prices (for the 2014 season): see ANGEL
\end{itemize}

\subsubsection*{Here's What To Do:}
\paragraph*{Team Sheet:}

The first sheet will relate the team’s batting effectiveness to winning and revenue.
\begin{enumerate}
\item Obtain team OBP and SLG from the team batting database above, along with team attendance, winning percentage, and the average team ticket price, from the appropriate links. 
\item Put the team name in the first column of the sheet, their regular season winning percentage in the second column, OBP in the third column, SLG in the fourth, and leave the fifth column blank for now. Then put attendance in the sixth column, ticket price in the seventh column, and calculate total team ticket revenue and put that in the eighth column.
 
\item The ``Economic Evaluation" article indicated that OBP was twice as important as SLG in producing runs and wins. Thus, create a simple ``index" of offensive production, $100*[2*OBP + SLG]$, and put that in your fifth column. Multiplying by 100 just makes the numbers easier to read and interpret. 
\item Create a scatterplot that relates the index you have just created, on the x-axis, to the team’s revenue, on the y-axis. Include the Excel trendline and display the equation of the trendline (this is an option in the chart design / layout tab). The slope of the trendline tells you how much each additional point of the index would be worth in revenue, in dollars. There is one “outlier” in your data. Delete this outlier and then use the coefficient estimate on the trendline as your estimate the value of increases in the index of team offensive production. 
\item Create a scatterplot that relates the index you have just created, on the x-axis, to the team’s winning percentage, on the y-axis. Include the Excel trendline and display the equation of the trendline (this is an option in the chart design / layout tab). If the index increases by 1, how much does the team’s winning percentage increase by? 
\item Create another scatterplot of win percentage (x-axis) and revenue (y-axis), and include the Excel trendline and the equation, as before. If the team’s winning percentage increases by 0.01, how much does the team’s revenue increase by?

\end{enumerate}

\paragraph*{Player Sheet:}

The second sheet of your spreadsheet will relate batter salaries to batter effectiveness.

\begin{enumerate}
\item Go to the list of free agents posted on ANGEL and choose 30 players at random. (All pitchers should have been taken off the list.) Work out a process you will use to pick your players–-it should be a random process-–and use it to choose your thirty. Please describe this process in your write-up. 
\item Obtain recent OBP and SLG for each player from the performance database above. You can just use the players’ 2014 statistics if you wish, but you can also include data from earlier seasons if you choose.
\item Obtain those players’ 2015 salaries from the salary database. 
\item Put the player name in the first column, their on-base percentage in the second column, their slugging average in the third column, and their 2015 salary in the fourth column. 
\item In the fifth column, calculate the player’s index of offensive production, just as you did above for the team. Then, calculate the player’s “marginal product”: the amount that they increase the team’s offensive index, instead of a player at the “Mendoza Line” of a .250 OBP and .300 SLG. Recognizing that the average starter takes 1/10 of all of the team’s at-bats, calculate the difference between each player’s index and the index of the Mendoza Line player, and then divide this by ten. 
\item Now, in the sixth column, put that player’s MRP–-the extra revenue the player brings in for the team. This is the value of the player’s marginal product and the value of increases in that marginal product, which you calculated on the previous sheet, plus the league minimum of \$400,000, for which we assume any team can get a player at the Mendoza Line. 
\item Make a scatterplot of your salary prediction vs. players' actual salaries. How close are they?
\end{enumerate}


\subsubsection*{Your Writeup:}
Write out, in three double-spaced, typewritten pages, 1) what you did, including writing out any formulas you utilized in your spreadsheet; 2) why you did it, including an explanation and justification of the formulas you used; and 3) conclusions. To write out the formulas, you may use traditional algebraic notation or copy the Excel formulas out of your spreadsheet. Attach a printout of your data and graphs to the end of your write-up. Everything should be combined together into a single file and follow the formatting specified at the top of this assignment. Take care to make your graphs clear and visually pleasing: in business, presentation matters. 

Bring a copy of your spreadsheet file to class if you are interested in discussing the assignment that day-–I will invite a couple of students to present their work to the class.

As with all assignments, you are invited to contact me by e-mail or phone with questions. Please get started early, that gives you time to work through any complications you may run into.



\subsection*{AII: Accessing the Simulator}
The simulator can be accessed upon request by visiting \href{www.MoneyballSimulator.info}{www.MoneyballSimulator.info}. A list of team sheets will be made available to choose from (ranging from current free agents to hall-of-famers).
\subsection*{AIII: Accessing Team Sheets}
A template of the team sheet, including only the Mendoza player is available in the documents section of the website to aid in the creation of custom draft boards. If you wish to create your own list of players to use with the simulator, simply create a new spreadsheet with the following \emph{unlabeled} columns (the columns MUST be in this order):
\begin{itemize}
\item ``Draft number": starts with 0 for the mendoza player (the first person on the draft board and default selection when salary cap is exceeded), and increases by one for every player on the draft board. Must be unique, and should not exceed (1-total number of players), including the mendoza player.
\item First Name
\item Last Name
\item Position (not used by program, but included for the students)
\item On-base Percentage
\item Slugging Average
\item Player Offensive index (2*OBP + SLG)
\item Column of zeroes (will be used by program)
\item Salary (without commas)
\end{itemize}
\subsection*{AIV: Using the \emph{Moneyball} Simulator Program}
This program simulates a simple draft of offensive players for a baseball team, and simulates a season of 10 games against every other team in the league. If playoffs are chosen, then the program also simulates a series of 7 games in each round between teams after seeding according to regular season record.

The goal of the program is to aid students in their understanding of Marginal Revenue Products by selecting players who are valuable relative to their wage, and to compete in an environment that is familiar to most students (sports). The program is compatible with the Windows operating system, and was written in Python. The program was inspired by the film \emph{Moneyball}, and written by the authors in order to be able to apply the principles demonstrated in the film in their sports and labor economics courses.

\subsubsection*{Instructions for Simulation}
\begin{enumerate}
\item Make sure that you have both the \emph{Moneyball} executable file \emph{and} the ``playersheet.csv" file in the same folder. If you don't, the program will not be able to run.
\item Open Moneyball.exe to begin the program.
\item Upon opening the program, you will be presented with a simple menu. Choose whether you would like to simulate a season with playoffs (an end-of-season seeded tournament to determine the league champion), or without. You are also given the option to quit the program.

\begin{figure}[ht!]
\centering
\includegraphics[width = 90mm]{images/intro} 
\end{figure}

\item After selecting a league format, you are prompted to indicate the number of teams that will participate in the season. Choose a number between 2 and 10. 

\begin{figure}[ht!]
\centering
\includegraphics[width = 90mm]{images/teamselect} 
\end{figure}

\item At this point, teams are randomly ordered for a 9-round draft. The team that selects last in one round will select first in the subsequent round.
\item As the draft proceeds, teams will be informed at the player selection prompt how much of their salary cap has been spent. The salary cap is currently set at \$20 Million. If a team exceeds the salary cap, all of their subsequent selections will be made by the simulator, and will be ``Mendoza Players."

\begin{figure}[ht!]
\centering
\includegraphics[width = 90mm]{images/draft} 
\end{figure}

\begin{figure}[ht!]
\centering
\includegraphics[width = 90mm]{images/draft2} 
\end{figure}

\item After the draft is concluded, the simulator presents team statistics for each team in the league, and pauses in order to allow discussion of what expected outcomes might be.

\begin{figure}[ht!]
\centering
\includegraphics[width = 90mm]{images/teamstats} 
\end{figure}

\item Following the team statistics, the game will then simulate the regular season, with each team playing all opponents 10 times. Regular season records are then presented, and the simulator pauses again to allow discussion of regular season outcomes. If you chose to play a season without a tournament, then the season concludes and a champion is declared.

\begin{figure}[ht!]
\centering
\includegraphics[width = 90mm]{images/regseason} 
\end{figure}

\item Using the number of wins in the regular season, the simulator seeds a tournament bracket and begins simulation of each round of the tournament. Each tournament round consists of 7 games against a single opponent. The simulation pauses after declaring the outcome of each match-up.

\begin{figure}[ht!]
\centering
\includegraphics[width = 90mm]{images/tourney} 
\end{figure}

\item Tournament play proceeds until the League Champion is declared. After pausing, the simulator returns to the start menu.

\end{enumerate}