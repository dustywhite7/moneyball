__author__ = 'dusty'

import random
import os
import operator
import pickle


try:
    datafile = open(os.path.join(os.getcwd(), 'playersheet.csv'), 'r')
    data = []
    salarycap = 20000000
    for row in datafile:
        data.append(row.strip().split(','))

    count = len(data)

    for x in range(count):
        for y in [0, 4, 5, 6, 7, 8]:
            try:
                data[x][y] = float(data[x][y])
            except:
                data[x][y] = None
except:
    print("No file named 'playersheet.csv'. \nPlease put this file into the same directory as the Moneyball program.\n")
    quit()

def tourneyinit():
    teams = int(input("How many teams will play? (Choose between 2 and 10)\n"))
    if (teams < 11) & (teams > 1):
        teams = int(teams)
    else:
        del teams
        tourneyinit()
    draft = [[]]
    draft2 = []
    for teamnum in range(teams):
        team = [teamnum+1, random.uniform(1, teams)]
        draft.append(team)
    draft.pop(0)
    draft.sort(key=lambda x: x[1])
    for teamnum in range(teams):
        draft2.append(draft[teamnum][0])
    return draft2

def draftplay(draft, roundpick = 0, position = 0, selections = None):
    payroll = [0]
    for x in range(1,max(draft)):
        payroll.append(0)
    draftflip = list(reversed(draft))
    selections = [[] for org in range(max(draft))]
    for roundpick in range(9):
        if (roundpick % 2) == 1:
            for team in draft:
                print("Round "+str(roundpick+1)+":\n")
                if payroll[team-1] < salarycap:
                    pick = chooseplayer(team, payroll)
                    print("Team " + str(team) + " selected " + str(data[pick][1]) + " " + str(data[pick][2]) + "\n")
                    selections[team-1].append(pick)
                    payroll[team-1] += data[pick][8]
                else:
                    print("Salary cap met or exceeded by team "+str(team)+". \nAdding Mendoza player to roster.\n")
                    selections[team-1].append(0)
                #pickle.dump(selections, open("selections.p", "wb"))
        else:
            for team in draftflip:
                print("Round "+str(roundpick+1)+":\n")
                if payroll[team-1] < salarycap:
                    pick = chooseplayer(team, payroll)
                    print("Team " + str(team) + " selected " + str(data[pick][1]) + " " + str(data[pick][2]) + "\n")
                    selections[team-1].append(pick)
                    payroll[team-1] += data[pick][8]
                else:
                    print("Salary cap met or exceeded by team "+str(team)+". \nAdding Mendoza player to roster.\n")
                    selections[team-1].append(0)
                    #pickle.dump(selections, open("selections.p", "wb"))
    return selections


def chooseplayer(team, payroll):
    print("Team "+str(team) +" will now choose.\nCurrent Payroll: $"+str(payroll[team-1])+"\n")
    choice = input("Which player will you add to your team?\n")
    try:
        choice = int(choice)
    except:
        print("Invalid player choice. Please make another selection.\n")
        choice = chooseplayer(team, payroll)
    try:
        if data[choice-1][7] == 0:
            data[choice-1][7] = 1
            return choice
        else:
            print("Player"+str(choice)+" already selected. Please make another selection.\n")
            choice = chooseplayer(team, payroll)
            return choice
    except:
        print("Invalid player choice. Please make another selection.\n")
        choice = chooseplayer(team, payroll)
        return choice

def buildleague(selections, draft):
    league = {}

    for x in range(max(draft)):
        league[x] = []
        for y in range(9):
            league[x].append(data[selections[x][y]])

    # for x in range(max(draft)):
    #     print("\n"+"Team "+str(x+1)+"\n")
    #     for y in range(9):
    #         print(league[x][y][1]+" "+league[x][y][2] +"\n")

    return league

def team(league, draft):
    teamstats = {}

    for x in range(max(draft)):
        teamstats[x] = [0, 0]
        for y in range(9):
            teamstats[x][0] += league[x][y][4]
            teamstats[x][1] += league[x][y][5]
        teamstats[x][0] /= 9
        teamstats[x][1] /= 9

    for x in range(max(draft)):
        print("\n"+"Team "+str(x+1)+"'s\n")
        print("OBP: "+str(teamstats[x][0])+"\n")
        print("SLG: "+str(teamstats[x][1])+"\n")
        temp = input()


    return teamstats


def series(teamstats1, teamstats2, games):
    win1 = .5 + 2.032 * (teamstats1[0] - teamstats2[0]) + .09 * (teamstats1[1] - teamstats2[1])
    results = [random.uniform(0, 1)]
    for x in range(1, games):
        random.seed()
        results.append(random.uniform(0, 1))
    wins = [0, 0]
    for x in range(games):
        if results[x] < win1:
            wins[0] += 1
        else:
            wins[1] += 1
    return wins


def season(teamstats, teams, draft):
    seasonrecord = [[1, 0, 0]]
    for x in range(1, teams):
        seasonrecord.append([x+1, 0, 0])
    for x in range(teams):
        for y in range(x+1, teams):
            record = series(teamstats[x], teamstats[y], 10)
            seasonrecord[x][1] += record[0]
            seasonrecord[y][1] += record[1]
    for x in range(teams):
        seasonrecord[x][2] = ((teams - 1) * 10) - seasonrecord[x][1]
    for x in range(max(draft)):
        print("\n"+"Team "+str(x+1)+"'s Record (Wins-Losses):\n")
        print(str(seasonrecord[x][1])+"-"+str(seasonrecord[x][2])+"\n")
        temp = input()
    return seasonrecord


def notourney():
    draft = tourneyinit()
    print(list(reversed(draft)))
    selections = draftplay(draft)
    league = buildleague(selections, draft)
    teamstats = team(league, draft)
    seasonresults = season(teamstats, max(draft), draft)
    seasonresults.sort(key=lambda x: x[1])
    print("\n\n\nTeam "+str(seasonresults[0][0])+" wins the league!\n")
    cleandraft()
    startprompt()



def withtourney():
    if os.path.isfile('draft-exclude.p'):
        use = input("Use previous draft order?\n1)Yes\n2)No")
        if use == 1:
            draft = pickle.load(open("draft.p", "rb"))
        elif use == 2:
            draft = tourneyinit()
            pickle.dump(draft, open("draft.p", "wb"))
        else:
            withtourney()
    else:
        draft = tourneyinit()
        pickle.dump(draft, open("draft.p", "wb"))
    print(list(reversed(draft)))
    selections = draftplay(draft)
    league = buildleague(selections, draft)
    teamstats = team(league, draft)
    seasonresults = season(teamstats, max(draft), draft)
    if max(draft) == 2:
        playoff = tourney2(teamstats, 1, 2)
        print("The League Champion is Team "+str(seasonresults[playoff-1][0])+"!\n")
        temp = input()
        cleandraft()
        startprompt()
    elif max(draft) == 3:
        champ = tourney3(teamstats, seasonresults)
        print("The League Champion is Team "+str(champ)+"!\n")
        temp = input()
        cleandraft()
        startprompt()
    elif max(draft) == 4:
        champ = tourney4(teamstats, seasonresults)
        print("The League Champion is Team "+str(champ)+"!\n")
        temp = input()
        cleandraft()
        startprompt()
    elif max(draft) == 5:
        champ = tourney5(teamstats, seasonresults)
        print("The League Champion is Team "+str(champ)+"!\n")
        temp = input()
        cleandraft()
        startprompt()
    elif max(draft) == 6:
        champ = tourney6(teamstats, seasonresults)
        print("The League Champion is Team "+str(champ)+"!\n")
        temp = input()
        cleandraft()
        startprompt()
    elif max(draft) == 7:
        champ = tourney7(teamstats, seasonresults)
        print("The League Champion is Team "+str(champ)+"!\n")
        temp = input()
        cleandraft()
        startprompt()
    elif max(draft) == 8:
        champ = tourney8(teamstats, seasonresults)
        print("The League Champion is Team "+str(champ)+"!\n")
        temp = input()
        cleandraft()
        startprompt()
    elif max(draft) == 9:
        champ = tourney9(teamstats, seasonresults)
        print("The League Champion is Team "+str(champ)+"!\n")
        temp = input()
        cleandraft()
        startprompt()
    else:
        champ = tourney10(teamstats, seasonresults)
        print("The League Champion is Team "+str(champ)+"!\n")
        temp = input()
        cleandraft()
        startprompt()



def tourney2(teamstats, team1, team2):
    results = series(teamstats[team1-1], teamstats[team2-1], 7)
    if results[0] > results[1]:
        return team1
    else:
        return team2

def tourney3(teamstats, seasonresults):
    seasonresults.sort(key=operator.itemgetter(2))
    team1 = seasonresults[0][0]
    team2 = seasonresults[1][0]
    team3 = seasonresults[2][0]
    print("Tournament seeding:\n1) Team "+str(team1)+"\n2) Team "+str(team2)+"\n3) Team "+str(team3)+"\n")
    temp = input()
    win = tourney2(teamstats, team2, team3)
    print("Team "+str(team2)+" vs Team "+str(team3)+"\n")
    print("Team "+str(win)+" advances to the Championship Game.\n")
    temp = input()
    champ = tourney2(teamstats, team1, win)
    return champ

def tourney4(teamstats, seasonresults):
    seasonresults.sort(key=operator.itemgetter(2))
    team1 = seasonresults[0][0]
    team2 = seasonresults[1][0]
    team3 = seasonresults[2][0]
    team4 = seasonresults[3][0]
    print("Tournament seeding:\n1) Team "+str(team1)+"\n2) Team "+str(team2)+"\n3) Team "+str(team3)+"\n4) Team "+str(team4)+"\n")
    temp = input()
    win1 = tourney2(teamstats, team2, team3)
    win2 = tourney2(teamstats, team1, team4)
    print("Team "+str(team2)+" vs Team "+str(team3)+"\n")
    print("Team "+str(win1)+" advances to the Championship Game.\n")
    temp = input()
    print("Team "+str(team1)+" vs Team "+str(team4)+"\n")
    print("Team "+str(win2)+" advances to the Championship Game.\n")
    temp = input()
    champ = tourney2(teamstats, win1, win2)
    return champ


def tourney5(teamstats, seasonresults):
    seasonresults.sort(key=operator.itemgetter(2))
    team1 = seasonresults[0][0]
    team2 = seasonresults[1][0]
    team3 = seasonresults[2][0]
    team4 = seasonresults[3][0]
    team5 = seasonresults[4][0]
    print("Tournament seeding:\n1) Team "+str(team1)+"\n2) Team "+str(team2)+"\n3) Team "+str(team3)+"\n4) Team " +
        str(team4)+"\n5) Team "+str(team5)+"\n")
    temp = input()
    win1 = tourney2(teamstats, team4, team5)
    print("Team "+str(team4)+" vs Team "+str(team5)+"\n")
    print("Team "+str(win1)+" wins the wild-card game.\n")
    temp = input()
    win2 = tourney2(teamstats, team1, win1)
    win3 = tourney2(teamstats, team2, team3)
    print("Team "+str(team1)+" vs Team "+str(win1)+"\n")
    print("Team "+str(win3)+" advances to the Championship Game.\n")
    temp = input()
    print("Team "+str(team2)+" vs Team "+str(team3)+"\n")
    print("Team "+str(win2)+" advances to the Championship Game.\n")
    temp = input()
    champ = tourney2(teamstats, win3, win2)
    return champ


def tourney6(teamstats, seasonresults):
    seasonresults.sort(key=operator.itemgetter(2))
    team1 = seasonresults[0][0]
    team2 = seasonresults[1][0]
    team3 = seasonresults[2][0]
    team4 = seasonresults[3][0]
    team5 = seasonresults[4][0]
    team6 = seasonresults[5][0]
    print("Tournament seeding:\n1) Team "+str(team1)+"\n2) Team "+str(team2)+"\n3) Team "+str(team3)+"\n4) Team " +
        str(team4)+"\n5) Team "+str(team5)+"\n6) Team "+str(team6)+"\n")
    temp = input()
    win1 = tourney2(teamstats, team6, team4)
    win2 = tourney2(teamstats, team3, team5)
    print("Team "+str(team4)+" vs Team "+str(team6)+"\n")
    print("Team "+str(win1)+" wins first round game.\n")
    temp = input()
    print("Team "+str(team3)+" vs Team "+str(team5)+"\n")
    print("Team "+str(win2)+" wins first round game.\n")
    temp = input()
    win3 = tourney2(teamstats, team1, win1)
    win4 = tourney2(teamstats, team2, win2)
    print("Team "+str(team1)+" vs Team "+str(win1)+"\n")
    print("Team "+str(win3)+" advances to the Championship Game.\n")
    temp = input()
    print("Team "+str(team2)+" vs Team "+str(win2)+"\n")
    print("Team "+str(win4)+" advances to the Championship Game.\n")
    temp = input()
    champ = tourney2(teamstats, win3, win4)
    return champ

def tourney7(teamstats, seasonresults):
    seasonresults.sort(key=operator.itemgetter(2))
    team1 = seasonresults[0][0]
    team2 = seasonresults[1][0]
    team3 = seasonresults[2][0]
    team4 = seasonresults[3][0]
    team5 = seasonresults[4][0]
    team6 = seasonresults[5][0]
    team7 = seasonresults[6][0]
    print("Tournament seeding:\n1) Team "+str(team1)+"\n2) Team "+str(team2)+"\n3) Team "+str(team3)+"\n4) Team " +
        str(team4)+"\n5) Team "+str(team5)+"\n6) Team "+str(team6)+"\n7) Team "+str(team7)+"\n")
    temp = input()
    win0 = tourney2(teamstats, team6, team7)
    print("Team "+str(team6)+" vs Team "+str(team7)+"\n")
    print("Team "+str(win0)+" wins wild-card game.\n")
    temp = input()
    win1 = tourney2(teamstats, win0, team4)
    win2 = tourney2(teamstats, team3, team5)
    print("Team "+str(team4)+" vs Team "+str(team6)+"\n")
    print("Team "+str(win1)+" wins first round game.\n")
    temp = input()
    print("Team "+str(team3)+" vs Team "+str(team5)+"\n")
    print("Team "+str(win2)+" wins first round game.\n")
    temp = input()
    win3 = tourney2(teamstats, team1, win1)
    win4 = tourney2(teamstats, team2, win2)
    print("Team "+str(team1)+" vs Team "+str(win1)+"\n")
    print("Team "+str(win3)+" advances to the Championship Game.\n")
    temp = input()
    print("Team "+str(team2)+" vs Team "+str(win2)+"\n")
    print("Team "+str(win4)+" advances to the Championship Game.\n")
    temp = input()
    champ = tourney2(teamstats, win3, win4)
    return champ

def tourney8(teamstats, seasonresults):
    seasonresults.sort(key=operator.itemgetter(2))
    team1 = seasonresults[0][0]
    team2 = seasonresults[1][0]
    team3 = seasonresults[2][0]
    team4 = seasonresults[3][0]
    team5 = seasonresults[4][0]
    team6 = seasonresults[5][0]
    team7 = seasonresults[6][0]
    team8 = seasonresults[7][0]
    print("Tournament seeding:\n1) Team "+str(team1)+"\n2) Team "+str(team2)+"\n3) Team "+str(team3)+"\n4) Team " +
        str(team4)+"\n5) Team "+str(team5)+"\n6) Team "+str(team6)+"\n7) Team "+str(team7)+"\n8) Team "+str(team8)+"\n")
    win1 = tourney2(teamstats, team1, team8)
    print("Team "+str(team1)+" vs Team "+str(team8)+"\n")
    print("Team "+str(win1)+" wins first round game.\n")
    temp = input()
    win2 = tourney2(teamstats, team2, team7)
    print("Team "+str(team2)+" vs Team "+str(team7)+"\n")
    print("Team "+str(win2)+" wins first round game.\n")
    temp = input()
    win3 = tourney2(teamstats, team3, team6)
    print("Team "+str(team3)+" vs Team "+str(team6)+"\n")
    print("Team "+str(win3)+" wins first round game.\n")
    temp = input()
    win4 = tourney2(teamstats, team4, team5)
    print("Team "+str(team4)+" vs Team "+str(team5)+"\n")
    print("Team "+str(win4)+" wins first round game.\n")
    temp = input()
    win5 = tourney2(teamstats, win1, win2)
    print("Team "+str(win1)+" vs Team "+str(win2)+"\n")
    print("Team "+str(win5)+" advances to the Championship Game.\n")
    temp = input()
    win6 = tourney2(teamstats, win3, win4)
    print("Team "+str(win3)+" vs Team "+str(win4)+"\n")
    print("Team "+str(win6)+" advances to the Championship Game.\n")
    temp = input()
    champ = tourney2(teamstats, win5, win6)
    return champ


def tourney9(teamstats, seasonresults):
    seasonresults.sort(key=operator.itemgetter(2))
    team1 = seasonresults[0][0]
    team2 = seasonresults[1][0]
    team3 = seasonresults[2][0]
    team4 = seasonresults[3][0]
    team5 = seasonresults[4][0]
    team6 = seasonresults[5][0]
    team7 = seasonresults[6][0]
    team8 = seasonresults[7][0]
    team9 = seasonresults[8][0]
    print("Tournament seeding:\n1) Team "+str(team1)+"\n2) Team "+str(team2)+"\n3) Team "+str(team3)+"\n4) Team " +
        str(team4)+"\n5) Team "+str(team5)+"\n6) Team "+str(team6)+"\n7) Team "+str(team7)+"\n8) Team "+str(team8) +
        "\n9) Team "+str(team9)+"\n")
    win0 = tourney2(teamstats, team9, team8)
    print("Team "+str(team9)+" vs Team "+str(team8)+"\n")
    print("Team "+str(win0)+" wins wild-card game.\n")
    temp = input()
    win1 = tourney2(teamstats, team1, win0)
    print("Team "+str(team1)+" vs Team "+str(win0)+"\n")
    print("Team "+str(win1)+" wins first round game.\n")
    temp = input()
    win2 = tourney2(teamstats, team2, team7)
    print("Team "+str(team2)+" vs Team "+str(team7)+"\n")
    print("Team "+str(win2)+" wins first round game.\n")
    temp = input()
    win3 = tourney2(teamstats, team3, team6)
    print("Team "+str(team3)+" vs Team "+str(team6)+"\n")
    print("Team "+str(win3)+" wins first round game.\n")
    temp = input()
    win4 = tourney2(teamstats, team4, team5)
    print("Team "+str(team4)+" vs Team "+str(team5)+"\n")
    print("Team "+str(win4)+" wins first round game.\n")
    temp = input()
    win5 = tourney2(teamstats, win1, win2)
    print("Team "+str(win1)+" vs Team "+str(win2)+"\n")
    print("Team "+str(win5)+" advances to the Championship Game.\n")
    temp = input()
    win6 = tourney2(teamstats, win3, win4)
    print("Team "+str(win3)+" vs Team "+str(win4)+"\n")
    print("Team "+str(win6)+" advances to the Championship Game.\n")
    temp = input()
    champ = tourney2(teamstats, win5, win6)
    return champ


def tourney10(teamstats, seasonresults):
    seasonresults.sort(key=operator.itemgetter(2))
    team1 = seasonresults[0][0]
    team2 = seasonresults[1][0]
    team3 = seasonresults[2][0]
    team4 = seasonresults[3][0]
    team5 = seasonresults[4][0]
    team6 = seasonresults[5][0]
    team7 = seasonresults[6][0]
    team8 = seasonresults[7][0]
    team9 = seasonresults[8][0]
    team10 = seasonresults[9][0]
    print("Tournament seeding:\n1) Team "+str(team1)+"\n2) Team "+str(team2)+"\n3) Team "+str(team3)+"\n4) Team " +
        str(team4)+"\n5) Team "+str(team5)+"\n6) Team "+str(team6)+"\n7) Team "+str(team7)+"\n8) Team "+str(team8)+
        "\n9) Team "+str(team9)+"\n10) Team "+str(team10)+"\n")
    win0 = tourney2(teamstats, team9, team10)
    print("Team "+str(team9)+" vs Team "+str(team10)+"\n")
    print("Team "+str(win0)+" wins wild-card game.\n")
    temp = input()
    win00 = tourney2(teamstats, team7, team8)
    print("Team "+str(team7)+" vs Team "+str(team8)+"\n")
    print("Team "+str(win00)+" wins wild-card game.\n")
    temp = input()
    win1 = tourney2(teamstats, team1, win0)
    print("Team "+str(team1)+" vs Team "+str(win0)+"\n")
    print("Team "+str(win1)+" wins first round game.\n")
    temp = input()
    win2 = tourney2(teamstats, team2, win00)
    print("Team "+str(team2)+" vs Team "+str(win00)+"\n")
    print("Team "+str(win2)+" wins first round game.\n")
    temp = input()
    win3 = tourney2(teamstats, team3, team6)
    print("Team "+str(team3)+" vs Team "+str(team6)+"\n")
    print("Team "+str(win3)+" wins first round game.\n")
    temp = input()
    win4 = tourney2(teamstats, team4, team5)
    print("Team "+str(team4)+" vs Team "+str(team5)+"\n")
    print("Team "+str(win4)+" wins first round game.\n")
    temp = input()
    win5 = tourney2(teamstats, win1, win3)
    print("Team "+str(win1)+" vs Team "+str(win3)+"\n")
    print("Team "+str(win5)+" advances to the Championship Game.\n")
    temp = input()
    win6 = tourney2(teamstats, win2, win4)
    print("Team "+str(win2)+" vs Team "+str(win4)+"\n")
    print("Team "+str(win6)+" advances to the Championship Game.\n")
    temp = input()
    champ = tourney2(teamstats, win5, win6)
    return champ



def startprompt():
    choose = input("Choose an option below:\n1) Season with Playoffs\n2) Season with NO Playoffs\n3) Quit\n")
    if int(choose) == 1:
        withtourney()
    elif int(choose) == 2:
        notourney()
    elif int(choose) == 3:
        quit()
    else:
        startprompt()

def cleandraft():
    for x in range(count):
        data[x][7] = 0


startprompt()
