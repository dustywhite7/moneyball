README

This program simulates a simple draft of offensive players for a baseball team, and simulates a season of 10 games against every other team in the league. If playoffs are chosen, then the program also simulates a series of 7 games in each round between teams after seeding according to regular season record.

The goal of the program is to aid students in their understanding of Marginal Revenue Products by selecting players who are valuable relative to their wage, and to compete in an environment that is familiar to most students (sports).

In order to use the python program, please rename the csv holding the player files you would like to use to “playersheet.csv”. Make sure that this cdv is in the same directory as moneyball.py. 

At this point, open Terminal, navigate to the folder containing moneyball.py, and execute the command “python moneyball.py”. The program will then initiate in the Terminal.